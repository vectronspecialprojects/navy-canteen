import vars from './env'

export const API_URI = vars.baseUrl
export const app_secret = vars.appSecret
export const app_token = vars.appToken
export const ERROR_STATUS = [401, 403, 410]
export const MaintainStatus = [503]
export const defaultDistance = 9999999999
export const version = '6.2.5'
export const fullVersion = `${version}.0.0`
export const backgroundImage = `${API_URI}background`
export const logoImage = `${API_URI}logo`
export const APP_LANGUAGE = 'APP_LANGUAGE'
export const APP_COLORS = 'APP_COLORS'
export const API_WHITE_LIST = [
  'userProfileSetting',
  'index.php',
  'member/profile',
  'accountSearchEmailMobile',
  'accountSearch',
  'instruction',
  'email/check',
  'account/deletion/confirm',
  'member/referralMessage'
]

export const SpecialPage = ['points', 'redeems', 'balance', 'saved']
