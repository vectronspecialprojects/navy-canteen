import {Platform} from 'react-native'

const env = {
  production: 'production',
  staging: 'staging'
}

const appSecret = '2da489f7ac6cce34ddcda02ef126270b'
const appToken = '12a3e1a732ee'

export const buildEvn = env.production

const baseUrl = {
  staging: 'https://apinavycanteen.vecport.net/navycanteen/api/',
  production: 'https://apinavycanteen.vecport.net/navycanteen/api/'
}

export const codePushKey = Platform.select({
  ios: {
    staging: 'u24tWukYzmVmuYtZfKiZ-XEhjbr-0tYqyZanz',
    production: 'u_egzyCxLdUAcQNv9Y3n08_zmgVwfNMIrC8fd',
  },
  android: {
    staging: '8o2_ZSgb64E4aFYpMCDNd5xY4rAqR7QX0zksz',
    production: 'fju9Ix-38Iz1f9O-uEc32WVe7mkrJdAc8GMHy',
  }
})

const vars = {
  googleApiKey: 'AIzaSyAl60Q1FOsJeZ4RGaF5KCp_Kcf9sGM6p3A',
  oneSignalApiKey: 'b217e596-08bc-4678-a67d-aca05bf70147',
  buildEvn: buildEvn,
  codePushKey: codePushKey[buildEvn],
  baseUrl: baseUrl[buildEvn],
  appSecret,
  appToken
}

export default vars
