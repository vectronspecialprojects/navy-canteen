import React from 'react'
import {Image, StyleSheet, TouchableOpacity} from 'react-native'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'
import {navigate, checkRouteOrigin} from './NavigationService'
import RouteKey from './RouteKey'
import {logoImage} from '../constants/constants'
import {HeroComponent} from '../components/ComponentContainer'

export const tabOptions = {
  tabBarLabelStyle: {
    fontFamily: Fonts.openSans,
    fontSize: 10
  },
  tabBarHideOnKeyboard: true,
  tabBarInactiveTintColor: Colors().heroText,
  tabBarActiveTintColor: Colors().heroFill,
  tabBarShowLabel: true,
  tabBarStyle: {
    backgroundColor: Colors().heroFill,
    paddingTop: 0
  }
}

export const defaultStackOptions = {
  headerStyle: {
    backgroundColor: Colors().heroFill
  },
  headerTitleStyle: {
    fontFamily: Fonts.openSans
  },
  headerBackTitleStyle: {
    fontFamily: Fonts.openSans
  },
  headerBackTitleVisible: false,
  headerTintColor: Colors().linkText,
  headerBackground: () => (
    <HeroComponent style={styles.logoContainer}>
      <TouchableOpacity
        onPress={() => {
          if (checkRouteOrigin() === 'Home') {
            navigate(RouteKey.HomeNavigator)
            navigate(RouteKey.Home)
          }
          if (checkRouteOrigin() === 'FrontpageNavigator') {
            navigate(RouteKey.FrontpageNavigator)
            navigate(RouteKey.StartupScreen)
          }
        }}
      >
        <Image style={styles.logo} source={{uri: logoImage}} />
      </TouchableOpacity>
    </HeroComponent>
  ),
  headerTitle: () => null
}

export const DrawerCtnOptions = {
  drawerActiveTintColor: Colors().linkText,
  drawerInactiveTintColor: Colors().linkText,
  drawerActiveBackgroundColor: null,
  drawerLabelStyle: {
    fontFamily: Fonts.openSans,
    fontSize: 16,
    color: Colors().linkText
  },
  drawerStyle: {
    flex: 1,
    width: '80%',
    backgroundColor: Colors().heroFill
  }
}

const styles = StyleSheet.create({
  logoContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  logo: {
    width: 100,
    height: 50,
    resizeMode: 'contain'
  }
})
