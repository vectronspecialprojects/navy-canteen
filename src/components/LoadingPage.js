import React from 'react'
import {View, ActivityIndicator, StyleSheet} from 'react-native'

import SubHeaderBar from './SubHeaderBar'
import Colors from '../Themes/Colors'
import ScreenContainer from './ScreenContainer'

const LoadingPage = props => {
  return (
    <ScreenContainer>
      <SubHeaderBar title={props.title} />
      <View style={styles.loadingContainer}>
        <ActivityIndicator size="large" color={Colors().linkText} />
      </View>
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default LoadingPage
