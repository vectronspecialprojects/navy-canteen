import React from 'react'
import {View, StyleSheet} from 'react-native'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Fonts from '../Themes/Fonts'
import ComponentContainer from './ComponentContainer'
import {CardText} from './Text'

function HistoryItem({data}) {
  const {order_details} = data || {}
  return (
    <ComponentContainer style={styles.container}>
      <View style={styles.row}>
        <CardText style={styles.text}>#{data?.id}</CardText>
        <CardText style={styles.text}>Total: {data?.total_price}</CardText>
      </View>
      {order_details?.map((item, index) => {
        return (
          <View style={styles.row} key={index.toString()}>
            <CardText style={[styles.text, {flex: 3}]}>{item?.voucher_name}</CardText>
            <View style={[styles.row, {flex: 2, marginBottom: 0}]}>
              <CardText style={styles.text}>Price: {item?.unit_price}</CardText>
              <CardText style={styles.text}>Qty: {item?.qty}</CardText>
            </View>
          </View>
        )
      })}
      <View style={styles.row}>
        <CardText style={styles.text}>{data?.ordered_at}</CardText>
        <CardText style={styles.text}>Status: {data?.order_status}</CardText>
      </View>
    </ComponentContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: responsiveWidth(10),
    marginTop: responsiveHeight(12),
    borderRadius: responsiveHeight(10),
    padding: responsiveWidth(10)
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: responsiveHeight(12)
  },
  text: {
    fontSize: responsiveFont(12),
    fontFamily: Fonts.regular
  }
})

export default HistoryItem
