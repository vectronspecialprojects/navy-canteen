import React from 'react'
import {StyleSheet, View} from 'react-native'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Colors from '../Themes/Colors'
import VectorIconButton from './VectorIconButton'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import {BackgroundText} from './Text'

export default function ({title, onPress, value, size = 22, style, textStyle}) {
  return (
    <View style={[styles.container, style]}>
      <VectorIconButton
        Component={MaterialIcons}
        name={value ? 'check-box' : 'check-box-outline-blank'}
        size={22}
        color={Colors().heroFill}
        style={{marginRight: responsiveWidth(10)}}
        onPress={onPress}
      />
      <BackgroundText style={[styles.title, textStyle]}>{title}</BackgroundText>
    </View>
  )
}

const styles = StyleSheet.create({
  title: {
    fontSize: responsiveFont(13),
    flex: 1
  },
  container: {
    flexDirection: 'row',
    marginBottom: responsiveHeight(15)
  }
})
