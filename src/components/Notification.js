import React, {useState} from 'react'
import {View, StyleSheet, TouchableOpacity} from 'react-native'
import {responsiveHeight, responsiveWidth, responsiveFont} from '../Themes/Metrics'
import * as infoServicesActions from '../store/actions/infoServices'
import {useDispatch, useSelector} from 'react-redux'
import Styles from '../Themes/Styles'
import Colors from '../Themes/Colors'
import {wait} from './UtilityFunctions'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Ionicons from 'react-native-vector-icons/Ionicons'
import HtmlTextPopup from './HtmlTextPopup'
import {isEmptyValues, isTrue} from '../utilities/utils'
import {navigate} from '../navigation/NavigationService'
import {CardText} from './Text'
import {shadow} from '../Themes/Metrics'

const Notification = props => {
  const appFlags = useSelector(state => state.app.appFlags)
  const [toggle, setToggle] = useState(false)
  const [isPopupVisible, setIsPopupVisible] = useState(false)
  const [showMsg, setShowMsg] = useState({})
  const dispatch = useDispatch()

  const notificationHandle = async id => {
    setIsPopupVisible(false)
    try {
      await dispatch(infoServicesActions.deleteNotification(id))
      if (id !== 0 && !isEmptyValues(showMsg)) {
        wait(400).then(() => navigate(JSON.parse(showMsg.notification.payload).react))
      }
    } catch (err) {
      console.log(err)
    } finally {
      setShowMsg({})
    }
  }

  return (
    <View
      style={{
        marginBottom: isTrue(appFlags?.isMultipleVenue) ? responsiveHeight(5) : 0,
        marginTop: isTrue(appFlags?.isMultipleVenue) ? 0 : responsiveHeight(15)
      }}
    >
      <View>
        <TouchableOpacity style={{flex: 1}} onPress={() => setToggle(!toggle)}>
          <View
            style={[
              styles.wrapper,
              {
                backgroundColor: Colors().backgroundFill,
                borderColor: Colors().backgroundFill
              }
            ]}
          >
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <FontAwesome name="bell-o" size={responsiveFont(24)} color={Colors().backgroundText} />
              <CardText
                style={[
                  Styles.xSmallBoldText,
                  {
                    marginLeft: responsiveWidth(10)
                  }
                ]}
                color={Colors().backgroundText}
              >
                You have got {props.notifications?.length} messages.
              </CardText>
            </View>
            <TouchableOpacity onPress={notificationHandle.bind(this, 0)}>
              <CardText
                style={[
                  Styles.xSmallUpBoldText,
                  {
                    marginRight: responsiveWidth(10)
                  }
                ]}
                color={Colors().backgroundText}
              >
                clear all
              </CardText>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </View>

      {toggle &&
        props.notifications.map(message => (
          <View key={message.id} style={{marginTop: responsiveHeight(5)}}>
            <TouchableOpacity
              style={{flex: 1}}
              onPress={() => {
                setShowMsg(message)
                setIsPopupVisible(true)
              }}
            >
              <View style={styles.container(Colors().cardFill, isTrue(appFlags.app_is_component_shadowed))}>
                <CardText
                  style={[
                    Styles.xSmallBoldText,
                    {
                      marginLeft: responsiveWidth(35)
                    }
                  ]}
                >
                  {message.notification.title}
                </CardText>
                <TouchableOpacity onPress={notificationHandle.bind(this, message.id)}>
                  <Ionicons name="close-sharp" size={responsiveFont(24)} color={Colors().cardText} />
                </TouchableOpacity>
              </View>
            </TouchableOpacity>
          </View>
        ))}
      <HtmlTextPopup
        isVisible={isPopupVisible}
        header={showMsg.notification?.title}
        text={showMsg.notification?.message}
        onOkPress={notificationHandle.bind(this, showMsg.id)}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    height: responsiveHeight(50),
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: responsiveWidth(12),
    justifyContent: 'space-between'
  },
  container: (backgroundColor, isShadow) => {
    let style = {
      height: responsiveHeight(60),
      marginHorizontal: responsiveWidth(15),
      flexDirection: 'row',
      alignItems: 'center',
      paddingHorizontal: responsiveWidth(12),
      justifyContent: 'space-between',
      borderRadius: responsiveWidth(15),
      backgroundColor
    }
    if (isShadow) {
      style = {
        ...style,
        ...shadow
      }
    }
    return style
  }
})

export default Notification
