import React from 'react'
import {TouchableOpacity} from 'react-native'
import Colors from '../Themes/Colors'

function VectorIconButton({
  Component,
  name,
  size,
  color,
  onPress,
  style,
  disabled,
  activeOpacity = 0,
  hitSlop
}) {
  return (
    <TouchableOpacity
      hitSlop={hitSlop}
      disabled={disabled}
      style={style}
      onPress={() => onPress?.()}
      activeOpacity={activeOpacity}
    >
      <Component name={name} size={size} color={color || Colors().heroText} />
    </TouchableOpacity>
  )
}

export default VectorIconButton
