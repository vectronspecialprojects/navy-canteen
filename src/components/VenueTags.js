import React, {useRef} from 'react'
import {View, Text, FlatList, StyleSheet} from 'react-native'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import {TouchableCmp} from './UtilityFunctions'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'
import ComponentContainer from './ComponentContainer'

const VenueTags = ({selectedTag, onPress, data}) => {
  const listRef = useRef()

  const renderItem = ({item, index}) => {
    const isSelected = selectedTag === item.id
    return (
      <View
        style={[
          styles.tags(
            isSelected ? Colors().venueTagActiveButtonFill : Colors().venueTagInactiveButtonFill,
            Colors().venueTagInactiveButtonBorder
          )
        ]}
      >
        <TouchableCmp
          style={styles.textWrapper}
          onPress={() => {
            onPress?.(item)
            listRef.current?.scrollToIndex({index: index, animated: true, viewPosition: 0.5})
          }}
        >
          <Text
            style={styles.textTag(
              isSelected ? Colors().venueTagActiveButtonText : Colors().venueTagInactiveButtonText
            )}
          >
            {item.name}
          </Text>
        </TouchableCmp>
      </View>
    )
  }
  return (
    <ComponentContainer>
      <FlatList
        ref={listRef}
        data={data}
        style={styles.list}
        showsHorizontalScrollIndicator={false}
        renderItem={renderItem}
        horizontal={true}
        keyExtractor={(item, index) => index.toString()}
      />
    </ComponentContainer>
  )
}

const styles = StyleSheet.create({
  tags: (backgroundColor, borderColor) => ({
    marginHorizontal: responsiveWidth(5),
    paddingHorizontal: responsiveWidth(10),
    borderRadius: 12,
    overflow: 'hidden',
    borderWidth: 2,
    backgroundColor,
    borderColor
  }),
  textTag: color => ({
    fontSize: responsiveFont(14),
    textTransform: 'capitalize',
    fontFamily: Fonts.openSansBold,
    alignSelf: 'center',
    color
  }),
  textWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  list: {
    height: responsiveHeight(50),
    paddingVertical: responsiveHeight(8)
  }
})

export default VenueTags
