import React from 'react'
import {StyleSheet} from 'react-native'
import Colors from '../Themes/Colors'
import {FontAwesomeTouch, FontAwesome5Touch} from './UtilityFunctions'
import {responsiveHeight, responsiveWidth, responsiveFont} from '../Themes/Metrics'
import CardContainer from './CardContainer'

const CartBar = props => {
  return (
    <CardContainer style={[styles.bar, props.style]}>
      <FontAwesomeTouch
        onPress={() => props.onCartPress?.()}
        name="shopping-cart"
        size={responsiveFont(22)}
        color={props.allowBooking ? Colors().cardText : Colors().gray}
        disabled={!props.allowBooking}
      />
      <FontAwesome5Touch
        onPress={props.onChatPress}
        name="rocketchat"
        size={responsiveFont(22)}
        color={props.allowChat ? Colors().cardText : Colors().gray}
        disabled={!props.allowChat}
      />
      <FontAwesomeTouch
        onPress={props.onFavoritePress}
        name="heart"
        size={responsiveFont(22)}
        color={props.favorite ? Colors().linkText : Colors().cardText}
      />
      <FontAwesome5Touch
        onPress={() => props.onSharePress?.()}
        name="share-alt"
        size={responsiveFont(22)}
        color={Colors().cardText}
      />
    </CardContainer>
  )
}

const styles = StyleSheet.create({
  bar: {
    flexDirection: 'row',
    width: '100%',
    paddingHorizontal: responsiveWidth(40),
    height: responsiveHeight(50),
    alignItems: 'center',
    justifyContent: 'space-between'
  }
})

export default CartBar
