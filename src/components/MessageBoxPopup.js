import React from 'react'
import {View, StyleSheet, TextInput, Keyboard, TouchableOpacity, KeyboardAvoidingView} from 'react-native'
import Metrics, {
  responsiveHeight,
  responsiveWidth,
  deviceWidth,
  deviceHeight,
  isIOS,
  FontSizes
} from '../Themes/Metrics'

import Modal from 'react-native-modal'
import Styles from '../Themes/Styles'
import Ionicons from 'react-native-vector-icons/Ionicons'
import {NegativeButton, PositiveButton} from './ButtonView'
import {DialogText, ErrorText} from './Text'
import {DialogComponent} from './ComponentContainer'
import Colors from '../Themes/Colors'

const MessageBoxPopup = props => {
  return (
    <Modal
      isVisible={props.isVisible}
      animationIn="swing"
      animationOut="zoomOut"
      onBackdropPress={() => Keyboard.dismiss()}
    >
      <KeyboardAvoidingView
        style={{flex: 1, justifyContent: 'center'}}
        behavior={isIOS() ? 'padding' : 'height'}
      >
        <DialogComponent style={styles.popupContainer}>
          <TouchableOpacity onPress={() => Keyboard.dismiss()} activeOpacity={1}>
            <View style={styles.titleContainer}>
              <DialogText style={styles.title} fontSize={FontSizes.large}>
                {props.header}
              </DialogText>
              <DialogText style={{marginTop: 5}} fontSize={FontSizes.small}>
                Got a question? Contact us below!
              </DialogText>
            </View>
            <View style={{padding: responsiveWidth(10)}}>
              <DialogText fontSize={FontSizes.large}>Message</DialogText>
              {props.value.length === 0 && (
                <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: responsiveHeight(2)}}>
                  <Ionicons
                    name="md-information-circle"
                    size={18}
                    color="red"
                    style={{marginRight: responsiveWidth(5)}}
                  />
                  <ErrorText fontSize={FontSizes.span}>message is required</ErrorText>
                </View>
              )}
              <View>
                <TextInput
                  style={styles.comment(
                    Colors().formInputFieldsText,
                    Colors().formInputFieldsBorder,
                    Colors().formInputFieldsFill
                  )}
                  value={props.value}
                  onChangeText={text => props.onChangeText(text)}
                  multiline={true}
                  underlineColorAndroid="transparent"
                />
              </View>
            </View>

            <View style={styles.popupButtonsBar}>
              <NegativeButton title={'Cancel'} style={styles.buttonBorder} onPress={props.onCancelPress} />
              <PositiveButton
                title={'Ok'}
                disabled={props.value.length === 0}
                style={styles.buttonBorder}
                onPress={props.onOkPress}
              />
            </View>
          </TouchableOpacity>
        </DialogComponent>
      </KeyboardAvoidingView>
    </Modal>
  )
}

const styles = StyleSheet.create({
  popupContainer: {
    borderRadius: 12,
    alignSelf: 'center',
    width: responsiveWidth(300),
    maxWidth: deviceWidth() * 0.7,
    overflow: 'hidden'
  },
  comment: (color, borderColor, backgroundColor) => ({
    paddingRight: responsiveWidth(3),
    paddingVertical: 5,
    color,
    height: responsiveHeight(200),
    width: '100%',
    maxHeight: deviceHeight() * 0.3,
    alignSelf: 'center',
    textAlignVertical: 'top',
    lineHeight: 23,
    borderWidth: StyleSheet.hairlineWidth,
    paddingHorizontal: Metrics.tiny,
    marginTop: Metrics.xxs,
    borderColor,
    backgroundColor
  }),
  popupButtonsBar: {
    width: '100%',
    flexDirection: 'row',
    height: responsiveHeight(50),
    marginBottom: responsiveHeight(10),
    justifyContent: 'space-between'
  },
  buttonBorder: {
    flex: 1,
    marginLeft: responsiveWidth(2),
    marginRight: responsiveWidth(1)
  },
  title: {
    ...Styles.largeCapBoldText,
    textTransform: 'uppercase',
    alignItems: 'flex-start'
  },
  titleContainer: {
    paddingHorizontal: responsiveWidth(10),
    paddingVertical: responsiveHeight(15),
    width: '100%'
  }
})

export default MessageBoxPopup
