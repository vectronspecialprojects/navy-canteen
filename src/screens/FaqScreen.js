import React, {useState, useCallback, useEffect, useMemo} from 'react'
import {FlatList} from 'react-native'
import {useDispatch, useSelector} from 'react-redux'
import * as infoServicesActions from '../store/actions/infoServices'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import SubHeaderBar from './../components/SubHeaderBar'
import Colors from '../Themes/Colors'
import FaqItem from './../components/FaqItem'
import SearchBar from './../components/SearchBar'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'
import ScreenContainer from '../components/ScreenContainer'
import ListEmpty from '../components/ListEmpty'
import CustomRefreshControl from '../components/CustomRefreshControl'

const FaqScreen = props => {
  const title = props.route.params.params?.page_name
  const [isRefreshing, setIsRefreshing] = useState(false)
  const dispatch = useDispatch()
  const faq = useSelector(state => state.infoServices.faq)
  const [search, setSearch] = useState('')

  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.fetchFaq())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    loadContent()
  }, [dispatch, loadContent])

  const showData = useMemo(() => {
    if (!faq) {
      return []
    }
    if (!search) {
      return faq
    }
    let key = search.toLowerCase()
    return faq.filter(
      item => item.question.toLowerCase().includes(key) || item.answer.toLowerCase().includes(key)
    )
  }, [search, faq])

  function renderItem({item}) {
    return <FaqItem title={item.question} content={item.answer} />
  }

  return (
    <ScreenContainer>
      <SubHeaderBar title={title} />
      <SearchBar
        onChangeText={text => setSearch(text)}
        style={{marginTop: 30}}
        backgroundColor={Colors().backgroundFill}
      />
      <FlatList
        data={showData}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
        ListEmptyComponent={<ListEmpty message={'No Faq found, please check again later'} />}
        refreshControl={
          <CustomRefreshControl
            refreshing={isRefreshing}
            onRefresh={() => {
              setIsRefreshing(true)
              loadContent()
            }}
          />
        }
      />
    </ScreenContainer>
  )
}

export default FaqScreen
