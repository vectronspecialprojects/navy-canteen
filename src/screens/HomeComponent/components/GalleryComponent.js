import {View, StyleSheet} from 'react-native'
import Styles from '../../../Themes/Styles'
import {deviceWidth, responsiveHeight} from '../../../Themes/Metrics'
import Swiper from 'react-native-swiper'
import FastImage from 'react-native-fast-image'
import React from 'react'
import {useSelector} from 'react-redux'
import Colors from '../../../Themes/Colors'

const GalleryComponent = ({
  height = responsiveHeight(135),
  autoplay = true,
  loop = true,
  showsPagination = true,
  style
}) => {
  const galleries = useSelector(state => state.app.galleries)
  return (
    <View style={[styles.container, {height}, style]}>
      <Swiper
        loop={loop}
        autoplay={autoplay}
        showsPagination={showsPagination}
        paginationStyle={styles.pagination}
        dotStyle={[{borderColor: Colors().heroFill}, styles.dot]}
        activeDotStyle={{backgroundColor: Colors().heroFill}}
      >
        {galleries.map((item, index) => {
          return <FastImage key={index.toString()} source={{uri: item}} style={[styles.image, {height}]} />
        })}
      </Swiper>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    ...Styles.centerContainer,
    marginBottom: responsiveHeight(40)
  },
  swipeContainer: {},
  image: {
    width: deviceWidth(),
    marginBottom: 20
  },
  dot: {
    backgroundColor: 'transparent',
    borderWidth: 1
  },
  pagination: {
    bottom: -responsiveHeight(15)
  }
})

export default GalleryComponent
