import React from 'react'
import {View, StyleSheet, Image} from 'react-native'
import {responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Colors from '../../../Themes/Colors'
import {useSelector} from 'react-redux'
import {logoImage} from '../../../constants/constants'
import {HeaderTitle} from '../../../components/Text'

function HeaderBackground() {
  const profile = useSelector(state => state.infoServices.profile)
  const useLegacyDesign = useSelector(state => state.app.useLegacyDesign)
  return (
    <View
      style={[
        styles.logoContainer,
        !useLegacyDesign && {
          alignItems: null,
          backgroundColor: Colors().heroFill
        }
      ]}
    >
      {!useLegacyDesign ? (
        <HeaderTitle style={styles.title}>Welcome back, {profile?.member?.first_name}</HeaderTitle>
      ) : (
        <Image style={styles.logo} source={{uri: logoImage}} />
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  logoContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingRight: responsiveWidth(20)
  },
  logo: {
    width: responsiveWidth(100),
    height: responsiveHeight(50),
    resizeMode: 'contain'
  },
  title: {
    marginBottom: responsiveHeight(10),
    marginLeft: responsiveWidth(15)
  }
})
export default HeaderBackground
