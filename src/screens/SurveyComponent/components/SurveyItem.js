import React from 'react'
import {View, StyleSheet} from 'react-native'
import Colors from '../../../Themes/Colors'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Fonts from '../../../Themes/Fonts'
import {TouchableCmp} from '../../../components/UtilityFunctions'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import ComponentContainer from '../../../components/ComponentContainer'
import {CardText} from '../../../components/Text'

function SurveyItem({data, onPress}) {
  return (
    <TouchableCmp onPress={onPress}>
      <ComponentContainer style={styles.container}>
        <FontAwesome name="question-circle-o" size={24} color={Colors().heroText} />
        <View style={{marginLeft: responsiveWidth(15), flex: 1}}>
          <CardText style={styles.title}>{data?.title}</CardText>
          {!!data?.point_reward && data?.reward_type === 'point' && (
            <CardText style={styles.content}>Get {data?.point_reward} point as a reward</CardText>
          )}
          {!!data?.voucher_setups?.name?.length && data?.reward_type !== 'point' && (
            <CardText style={styles.content}>Get {data?.voucher_setups?.name} voucher as a reward</CardText>
          )}
        </View>
        <MaterialIcons name="chevron-right" size={24} color={Colors().cardText} />
      </ComponentContainer>
    </TouchableCmp>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginHorizontal: responsiveWidth(10),
    marginTop: responsiveHeight(17),
    padding: responsiveWidth(15),
    borderRadius: responsiveHeight(10),
    alignItems: 'center'
  },
  title: {
    fontSize: responsiveFont(14),
    fontFamily: Fonts.openSansBold
  },
  content: {
    fontSize: responsiveFont(14),
    fontFamily: Fonts.openSans
  }
})

export default SurveyItem
