import React from 'react'
import {StyleSheet, TouchableOpacity, Text} from 'react-native'
import Colors from '../../../Themes/Colors'
import {responsiveFont, responsiveHeight} from '../../../Themes/Metrics'
import Fonts from '../../../Themes/Fonts'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

function MenuItem({title, color, route, params, onPress}) {
  return (
    <TouchableOpacity style={[styles.container, {borderColor: Colors().gray}]} onPress={onPress}>
      <Text style={[styles.title, {color}]}>{title}</Text>
      <MaterialIcons name={'chevron-right'} size={25} color={color} />
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    height: responsiveHeight(45),
    alignItems: 'center',
    flexDirection: 'row'
  },
  title: {
    fontSize: responsiveFont(14),
    fontFamily: Fonts.openSans,
    flex: 1
  }
})
export default MenuItem
