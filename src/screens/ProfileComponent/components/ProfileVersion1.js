import React from 'react'
import {View, StyleSheet, ScrollView} from 'react-native'
import {responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Colors from '../../../Themes/Colors'
import Avatar from '../../../components/Avatar'
import {EntypoTouch} from '../../../components/UtilityFunctions'
import RouteKey from '../../../navigation/RouteKey'
import UpdateAccountScreen from '../UpdateAccountScreen'
import {isTrue} from '../../../utilities/utils'
import {useSelector} from 'react-redux'

function ProfileVersion1({updateImage, setUpdateImage, profile, navigation, handleUpdateAvatar}) {
  const appFlags = useSelector(state => state.app.appFlags)
  return (
    <View style={styles.container}>
      <View style={{height: responsiveHeight(100), backgroundColor: Colors().heroFill}}>
        <Avatar
          uri={updateImage?.path || profile?.member?.profile_img}
          size={responsiveWidth(128)}
          style={styles.avatar}
          editable={true}
          onImageSelect={image => {
            handleUpdateAvatar(image)
            setUpdateImage(image)
          }}
          showSpinner={isTrue(appFlags?.allowed_member_photo_spin)}
        />
      </View>
      <View style={styles.actionContainer}>
        <EntypoTouch
          name="heart-outlined"
          size={30}
          color={Colors().cardText}
          onPress={() => {
            navigation.navigate(RouteKey.FavoriteScreen)
          }}
        />
        <EntypoTouch
          name="back-in-time"
          size={30}
          color={Colors().cardText}
          onPress={() => {
            navigation.navigate(RouteKey.HistoryScreen)
          }}
        />
      </View>
      <ScrollView>
        <UpdateAccountScreen />
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  avatar: {
    alignSelf: 'center',
    bottom: responsiveWidth(-64),
    position: 'absolute',
    zIndex: 999
  },
  buttonBorder: {
    borderWidth: 2,
    marginBottom: responsiveHeight(8)
  },
  buttonContainer: {
    marginTop: responsiveHeight(68),
    marginBottom: responsiveHeight(20),
    paddingHorizontal: responsiveWidth(12)
  },
  actionContainer: {
    height: responsiveHeight(80),
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: responsiveWidth(20),
    paddingTop: responsiveHeight(10)
  }
})
export default ProfileVersion1
