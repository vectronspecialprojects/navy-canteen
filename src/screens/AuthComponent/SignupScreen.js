import React, {useReducer, useCallback, useState, useEffect} from 'react'
import {View, StyleSheet, KeyboardAvoidingView, ScrollView} from 'react-native'
import {useDispatch, useSelector} from 'react-redux'
import {isIOS, responsiveFont, responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import * as authServicesActions from '../../store/actions/authServices'
import Fonts from '../../Themes/Fonts'
import {PositiveButton} from '../../components/ButtonView'
import TermAndCondition from '../../components/TermAndCondition'
import {
  getKeyboardVerticalOffset,
  getStateFromPostCode,
  isTrue,
  parseStringToJson
} from '../../utilities/utils'
import * as api from '../../utilities/ApiManage'
import Alert from '../../components/Alert'
import {localize} from '../../locale/I18nConfig'
import CheckBox from '../../components/CheckBox'
import ProfileFieldInput from '../../components/ProfileFieldInput'
import SubHeaderBar from '../../components/SubHeaderBar'
import ScreenContainer from '../../components/ScreenContainer'
import {Link} from '../../components/Text'

const BY_VENUE = 'by_venue'
const BY_VENUE_TAG = 'by_venue_tag'
const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE'
const GAMING = {
  odyssey: 'odyssey',
  igt: 'igt'
}

const formReducer = (state, action) => {
  if (action.type === FORM_INPUT_UPDATE) {
    const updatedValues = {
      ...state.inputValues,
      [action.input]: action.value
    }
    const updatedValidities = {
      ...state.inputValidities,
      [action.input]: action.isValid
    }
    let updatedFormIsValid = true
    for (const key in updatedValidities) {
      updatedFormIsValid = updatedFormIsValid && updatedValidities[key]
    }
    return {
      formIsValid: updatedFormIsValid,
      inputValidities: updatedValidities,
      inputValues: updatedValues
    }
  }
  return state
}

const SignupScreen = props => {
  const dispatch = useDispatch()
  const memberDefaultTierDetail = useSelector(state => state.app.memberDefaultTierDetail)
  const memberDefaultTierOption = useSelector(state => state.app.memberDefaultTierOption)
  const appFlags = useSelector(state => state.app.appFlags)
  const [listTier, setListTier] = useState('')
  const [options, setOptions] = useState([])
  const [gamingFields, setGamingFields] = useState([])
  const [optOutMarketing, setOptOutMarketing] = useState(false)
  const [agreeTermAndCondition, setAgreeTermCondition] = useState(false)

  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {},
    inputValidities: {},
    formIsValid: false
  })

  const getProfileSetting = useCallback(async () => {
    try {
      dispatch(setGlobalIndicatorVisibility(true))
      const response = await api.getUserProfileSetting()
      let hasTier = false
      if (!response.ok) {
        throw new Error(response?.message)
      }

      let list = parseStringToJson(response?.bepozcustomfield?.extended_value)
      list?.map(e => {
        dispatchFormState({
          type: FORM_INPUT_UPDATE,
          input: e?.id,
          value: '',
          isValid: !e.required
        })
        if (e.fieldType === 'Tier') {
          hasTier = true
        }
      })
      if (hasTier) {
        setListTier(response.data)
      }
      list = list.filter(item => item.displayInApp).sort((a, b) => +a.displayOrder - +b.displayOrder)
      setOptions(list)
    } catch (e) {
      Alert.alert(localize('signUp.titleGetTier'), e.message, [{text: localize('okay')}])
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  useEffect(() => {
    getProfileSetting()
    if (isTrue(appFlags?.gaming_system_enable)) {
      getGaming()
    }
  }, [appFlags?.gaming_system_enable, getProfileSetting])

  const inputChangeHandler = useCallback(
    (inputIdentifier, inputValue, inputValidity) => {
      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        input: inputIdentifier,
        value: inputValue,
        isValid: inputValidity
      })
    },
    [dispatchFormState]
  )

  useEffect(() => {
    if (memberDefaultTierOption === BY_VENUE && formState.inputValues?.venue?.value) {
      let tierId = +memberDefaultTierDetail?.[formState.inputValues?.venue?.value]?.tier
      if (tierId) {
        inputChangeHandler('tier', tierId, true)
      }
    }
  }, [
    formState.inputValues?.venue?.value,
    inputChangeHandler,
    memberDefaultTierDetail,
    memberDefaultTierOption
  ])

  useEffect(() => {
    if (formState.inputValues?.post_code?.length >= 3) {
      const state = getStateFromPostCode(+formState.inputValues?.post_code)
      inputChangeHandler('state', state, true)
      if (memberDefaultTierOption === BY_VENUE_TAG && state) {
        const tierId = +memberDefaultTierDetail?.[state]?.tier
        if (tierId) {
          inputChangeHandler('tier', tierId, true)
        }
      }
    }
  }, [formState.inputValues?.post_code, inputChangeHandler, memberDefaultTierDetail, memberDefaultTierOption])

  async function getGaming() {
    try {
      const response = await api.apiGetGaming()
      if (!response.ok) {
        throw new Error(response?.message)
      }
      let list = parseStringToJson(response?.bepozcustomfield?.extended_value)
      list = list.filter(item => isTrue(item.displayInApp)).sort((a, b) => +a.displayOrder - +b.displayOrder)
      list?.map(e => {
        if (e.required) {
          dispatchFormState({
            type: FORM_INPUT_UPDATE,
            input: e?.id,
            value: '',
            isValid: false
          })
        }
      })
      setGamingFields(list)
    } catch (e) {
      Alert.alert(localize('signUp.titleGetGaming'), e.message, [{text: localize('okay')}])
    }
  }

  const signupHandler = async () => {
    dispatch(setGlobalIndicatorVisibility(true))
    let data = {
      ...formState.inputValues,
      venue_id: formState.inputValues.venue_id?.value,
      share_info: true,
      opt_out_marketing: optOutMarketing
    }
    try {
      if (isTrue(appFlags?.gaming_system_enable) && appFlags?.gaming_system === GAMING.odyssey) {
        await dispatch(authServicesActions.signupOdyssey(data))
      } else if (isTrue(appFlags?.gaming_system_enable) && appFlags?.gaming_system === GAMING.igt) {
        await dispatch(authServicesActions.signupIGT(data))
      } else {
        await dispatch(authServicesActions.signup(data))
      }
    } catch (err) {
      Alert.alert(localize('signUp.titleErrSignUp'), err.message, [{text: localize('okay')}])
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  const renderSignUpFields = useCallback(() => {
    return options?.map((item, index) => {
      return (
        <ProfileFieldInput
          key={index.toString()}
          type={item.fieldType}
          item={item}
          listTier={listTier}
          formState={formState}
          inputChangeHandler={inputChangeHandler}
        />
      )
    })
  }, [options, listTier, formState, inputChangeHandler])

  const renderGaming = useCallback(() => {
    return gamingFields?.map((item, index) => {
      return (
        <ProfileFieldInput
          key={index.toString()}
          type={item.fieldType}
          item={item}
          listTier={listTier}
          formState={formState}
          inputChangeHandler={inputChangeHandler}
        />
      )
    })
  }, [formState, gamingFields, inputChangeHandler, listTier])

  return (
    <ScreenContainer>
      <SubHeaderBar title={localize('signUp.title')} alignLeft />
      <KeyboardAvoidingView
        style={{paddingHorizontal: responsiveWidth(20), flex: 1}}
        behavior={isIOS() ? 'padding' : null}
        keyboardVerticalOffset={getKeyboardVerticalOffset()}
      >
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          showsVerticalScrollIndicator={false}
          style={{paddingTop: responsiveHeight(10)}}
        >
          {renderSignUpFields()}
          {isTrue(appFlags?.gaming_system_enable) && renderGaming()}
          <View style={{marginTop: responsiveHeight(20)}}>
            <View style={{marginTop: responsiveHeight(12)}}>
              <Link style={styles.checkBoxTitle}>No thanks, I don’t want exclusive offers</Link>
              <CheckBox
                title={localize('signUp.receiveOffer')}
                value={optOutMarketing}
                onPress={() => setOptOutMarketing(!optOutMarketing)}
              />
            </View>
            <Link style={styles.checkBoxTitle}>Terms & Conditions</Link>
            <TermAndCondition
              onCheck={() => setAgreeTermCondition(!agreeTermAndCondition)}
              value={agreeTermAndCondition}
              allowCheck={true}
            />
          </View>
          <View style={styles.buttonContainer}>
            <PositiveButton
              title={localize('signUp.continue')}
              onPress={signupHandler}
              disabled={!formState.formIsValid || !agreeTermAndCondition}
            />
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  buttonContainer: {
    marginTop: responsiveHeight(15),
    marginBottom: responsiveHeight(30)
  },
  selectVenueInput: {
    fontSize: responsiveFont(14),
    fontFamily: Fonts.openSans
  },
  checkBoxTitle: {
    fontFamily: Fonts.openSansBold,
    fontSize: responsiveFont(16),
    marginBottom: responsiveHeight(5)
  }
})

export default SignupScreen
