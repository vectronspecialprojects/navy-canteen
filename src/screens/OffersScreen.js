import React, {useState, useEffect, useCallback, useMemo} from 'react'
import {FlatList} from 'react-native'
import {useDispatch, useSelector} from 'react-redux'
import * as infoServicesActions from '../store/actions/infoServices'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import OffersScreenPopup from './../components/OffersScreenPopup'
import VoucherTile from './../components/VoucherTile'
import analytics from '@react-native-firebase/analytics'
import Toast from '../components/Toast'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'
import ButtonVenue from '../components/ButtonVenue'
import ScreenContainer from '../components/ScreenContainer'
import ListEmpty from '../components/ListEmpty'
import CustomRefreshControl from '../components/CustomRefreshControl'

const OffersScreen = props => {
  const dispatch = useDispatch()
  const [isRefreshing, setIsRefreshing] = useState(false)
  const selectedId = useSelector(state => state.infoServices.preferredVenueId)
  const [isPopupVisible, setIsPopupVisible] = useState(false)
  const [item, setItem] = useState(null)
  const offers = useSelector(state => state.infoServices.offers)

  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.fetchOffers())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    loadContent()
  }, [dispatch, loadContent])

  useEffect(() => {
    async function firebase() {
      await analytics().logScreenView({screen_name: 'Promotions'})
    }

    firebase()
  }, [])

  const claimOffer = useCallback(async () => {
    setIsPopupVisible(false)
    dispatch(setGlobalIndicatorVisibility(true))
    try {
      const rest = await dispatch(infoServicesActions.claimOffer(item))
      Toast.success(rest)
      loadContent()
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
      setItem(null)
    }
  }, [dispatch, item, loadContent])

  const showData = useMemo(() => {
    if (offers === undefined) {
      return []
    }
    if (offers) {
      if (selectedId === 0) {
        return offers.sort((a, b) => {
          return a.listing.display_order - b.listing.display_order
        })
      }
      return offers
        .filter(data => data.listing.venue.id === 0 || data.listing.venue.id === selectedId)
        .sort((a, b) => {
          return a.listing.display_order - b.listing.display_order
        })
    }
  }, [offers, selectedId])

  const popupShow = item => {
    if (item === 'close') {
      setIsPopupVisible(false)
    } else {
      setIsPopupVisible(true)
      setItem(item)
    }
  }

  const renderGridItem = itemData => {
    return (
      <VoucherTile
        imgSource={itemData.item.listing.image_square}
        onPress={() => popupShow(itemData.item.listing)}
        title={itemData.item.listing.heading}
      />
    )
  }

  return (
    <ScreenContainer>
      <ButtonVenue />
      <FlatList
        numColumns={2}
        columnWrapperStyle={{
          flex: 1,
          justifyContent: 'space-evenly'
        }}
        keyExtractor={item => item.id}
        data={showData}
        renderItem={renderGridItem}
        ListEmptyComponent={<ListEmpty message={localize('offerScreen.messageNotFound')} />}
        refreshControl={
          <CustomRefreshControl
            refreshing={isRefreshing}
            onRefresh={() => {
              setIsRefreshing(true)
              loadContent()
            }}
          />
        }
      />

      <OffersScreenPopup
        isVisible={isPopupVisible}
        renderItem={item}
        onCancelPress={() => popupShow('close')}
        onOkPress={claimOffer}
      />
    </ScreenContainer>
  )
}

export default OffersScreen
