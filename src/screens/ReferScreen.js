import React, {useMemo, useState, useCallback, useEffect} from 'react'
import {View, StyleSheet, ScrollView} from 'react-native'
import * as infoServicesActions from '../store/actions/infoServices'
import SubHeaderBar from './../components/SubHeaderBar'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import TextInputView from './../components/TextInputView'
import {PositiveButton} from './../components/ButtonView'
import {useDispatch, useSelector} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import {apiSubmitRefer} from '../utilities/ApiManage'
import Toast from '../components/Toast'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'
import {validateEmail} from '../utilities/utils'
import Fonts from '../Themes/Fonts'
import ScreenContainer from '../components/ScreenContainer'
import {CardText} from '../components/Text'

const ReferScreen = ({navigation}) => {
  const [fullName, setFullName] = useState('')
  const [email, setEmail] = useState('')
  const msg = useSelector(state => state.infoServices.referMsg)
  const emailNotAllowedDomains = useSelector(state => state.app.emailNotAllowedDomains)
  const dispatch = useDispatch()

  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.fetctReferMsg())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    loadContent()
  }, [dispatch, loadContent])

  async function handleSubmit() {
    try {
      dispatch(setGlobalIndicatorVisibility(true))
      const res = await apiSubmitRefer(email, fullName)
      if (!res.ok) {
        throw new Error(res.message)
      }
      Toast.success(localize('refer.invitationSuccess'))
      navigation.navigate('Home')
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  const isAllow = useMemo(() => {
    return fullName && email
  }, [fullName, email])

  const isEmailValid = useMemo(() => {
    if (!validateEmail(email)) {
      return false
    }
    return !(!!emailNotAllowedDomains && emailNotAllowedDomains.test(email))
  }, [email, emailNotAllowedDomains])

  return (
    <ScreenContainer>
      <SubHeaderBar title={localize('refer.title')} />
      <View style={styles.container}>
        <CardText style={styles.content}>{msg}</CardText>
        <ScrollView bounces={false}>
          <TextInputView
            floatTitle={localize('refer.fullName')}
            onChangeText={text => setFullName(text)}
            value={fullName}
          />
          <TextInputView
            keyboardType={'email-address'}
            floatTitle={localize('refer.email')}
            onChangeText={text => setEmail(text)}
            value={email}
            messageError={!isEmailValid && localize('refer.invalidEmail')}
          />
        </ScrollView>
        <PositiveButton
          title={localize('refer.continue')}
          disabled={!isAllow || !isEmailValid}
          style={{marginTop: responsiveHeight(34)}}
          onPress={() => handleSubmit()}
        />
      </View>
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: responsiveWidth(20)
  },
  content: {
    fontSize: responsiveFont(12),
    textAlign: 'center',
    marginBottom: responsiveHeight(40),
    marginTop: responsiveHeight(10),
    fontFamily: Fonts.openSans
  }
})

export default ReferScreen
