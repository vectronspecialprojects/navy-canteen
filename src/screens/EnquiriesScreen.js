import React from 'react'
import {View, ScrollView} from 'react-native'
import SubHeaderBar from './../components/SubHeaderBar'
import {localize} from '../locale/I18nConfig'
import ScreenContainer from '../components/ScreenContainer'

const EnquiriesScreen = props => {
  return (
    <ScreenContainer>
      <SubHeaderBar title={localize('enquiry.title')} />
      <View style={{flex: 1}}>
        <ScrollView />
      </View>
    </ScreenContainer>
  )
}

export default EnquiriesScreen
