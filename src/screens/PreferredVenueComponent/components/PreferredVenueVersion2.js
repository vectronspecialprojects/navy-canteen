import React, {useState} from 'react'
import {StyleSheet, FlatList} from 'react-native'
import Metrics from '../../../Themes/Metrics'
import VenueItem from './VenueItem'
import {isTrue} from '../../../utilities/utils'
import VenueTags from '../../../components/VenueTags'
import {useSelector} from 'react-redux'
import ListEmpty from '../../../components/ListEmpty'
import CustomRefreshControl from '../../../components/CustomRefreshControl'
import ScreenContainer from '../../../components/ScreenContainer'

function PreferredVenueVersion2({
  profile,
  navigation,
  venueTagsSetting,
  venueTagShow,
  isRefreshing,
  loadContent,
  savePreferredVenue
}) {
  const [selectedTag, setSelectedTag] = useState(venueTagShow[0])
  const appFlags = useSelector(state => state.app.appFlags)

  function renderItem({item, index}) {
    return (
      <VenueItem
        imgSource={item?.image}
        venueName={item?.name}
        venueAddress={item?.address}
        onPress={() => savePreferredVenue(item?.id)}
        distance={item?.distance}
      />
    )
  }

  return (
    <ScreenContainer>
      {isTrue(venueTagsSetting?.isTagEnabled) &&
        isTrue(appFlags?.isMultipleVenue) &&
        !!venueTagShow?.length && (
          <VenueTags
            data={venueTagShow}
            selectedTag={selectedTag?.id}
            onPress={venueTag => setSelectedTag(venueTag)}
          />
        )}
      <FlatList
        style={styles.listContainer}
        data={selectedTag?.listVenue}
        renderItem={renderItem}
        keyExtractor={item => item.id.toString()}
        ListEmptyComponent={<ListEmpty message={'No venues found, please check again later.'} />}
        refreshControl={<CustomRefreshControl refreshing={isRefreshing} onRefresh={loadContent} />}
      />
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  listContainer: {
    marginTop: Metrics.xs
  }
})
export default PreferredVenueVersion2
