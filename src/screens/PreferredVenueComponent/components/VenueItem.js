import React, {useMemo} from 'react'
import {View, StyleSheet} from 'react-native'
import {useSelector} from 'react-redux'
import {TouchableCmp} from '../../../components/UtilityFunctions'
import {FontSizes, isIOS, responsiveHeight, responsiveWidth, shadow} from '../../../Themes/Metrics'
import FastImage from 'react-native-fast-image'
import Images from '../../../Themes/Images'
import {defaultDistance} from '../../../constants/constants'
import {CardText} from '../../../components/Text'
import ComponentContainer from '../../../components/ComponentContainer'
import {isTrue} from '../../../utilities/utils'
import Colors from '../../../Themes/Colors'

function VenueItem({venueAddress, onPress, imgSource, venueName, distance}) {
  const useLegacyDesign = useSelector(state => state.app.useLegacyDesign)
  const appFlags = useSelector(state => state.app.appFlags)
  const address = useMemo(() => {
    const after = venueAddress?.slice(venueAddress.indexOf(',') + 1)?.trim()
    const before = venueAddress?.slice(0, venueAddress.indexOf(','))?.trim()
    return {
      after,
      before
    }
  }, [venueAddress])
  return (
    <TouchableCmp onPress={onPress}>
      <View style={isTrue(appFlags.app_is_component_shadowed) && shadow}>
        <ComponentContainer
          style={[
            styles.container,
            !useLegacyDesign && {
              ...styles.newDesign
            },
            {backgroundColor: Colors().cardFill},
            !isIOS() && isTrue(appFlags.app_is_component_shadowed) && shadow
          ]}
        >
          <FastImage
            source={imgSource ? {uri: imgSource} : Images.defaultImage}
            style={styles.image}
            resizeMode={'contain'}
          />
          <View style={styles.textWrapper}>
            <CardText fontSize={FontSizes.body}>{venueName}</CardText>
            <CardText style={styles.address} fontSize={FontSizes.small}>
              {address?.before}
            </CardText>
            <CardText fontSize={FontSizes.small}>{address?.after}</CardText>
            {distance !== defaultDistance && (
              <CardText fontSize={FontSizes.small}>{distance?.toFixed(2)} Km</CardText>
            )}
          </View>
        </ComponentContainer>
      </View>
    </TouchableCmp>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginBottom: responsiveHeight(12),
    alignItems: 'center'
  },
  image: {
    width: responsiveWidth(100),
    aspectRatio: 1
  },
  textWrapper: {
    flex: 1,
    height: '100%',
    paddingVertical: responsiveHeight(5),
    paddingHorizontal: responsiveWidth(15)
  },
  address: {
    marginTop: responsiveHeight(10),
    lineHeight: responsiveHeight(20)
  },
  newDesign: {
    marginHorizontal: responsiveWidth(10),
    borderRadius: responsiveWidth(15),
    overflow: 'hidden'
  }
})

export default VenueItem
