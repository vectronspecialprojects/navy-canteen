import React, {useCallback, useEffect, useImperativeHandle, useState} from 'react'
import {View, StyleSheet, Modal, TouchableOpacity, Text, Switch, ScrollView, FlatList} from 'react-native'
import Colors from '../../../Themes/Colors'
import {deviceWidth, responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Fonts from '../../../Themes/Fonts'
import {apiGetListCard} from '../../../utilities/ApiManage'
import TextInputView from '../../../components/TextInputView'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import ButtonView, {NegativeButton, PositiveButton} from '../../../components/ButtonView'
import BottomSheet from '../../../components/BottomSheet'
import {formatCardNumber} from '../../../utilities/utils'
import {CardText} from '../../../components/Text'
import ComponentContainer from '../../../components/ComponentContainer'

function PaymentPopup({visible, onClose, amount, onSubmit}, ref) {
  const [listCard, setListCard] = useState('')
  const [name, setName] = useState('')
  const [number, setNumber] = useState('')
  const [expiryDate, setExpiryDate] = useState('')
  const [cvc, setCvc] = useState('')
  const [isSave, setSave] = useState(false)
  const [isUseSaveCard, setUseSaveCard] = useState(false)
  const [errorMessage, setErrorMessage] = useState('')
  const [isAddNewCard, setAddNewCard] = useState(false)
  const [token, setToken] = useState('')
  const [brand, setBrand] = useState('')
  const [showSelectCard, setShowSelectCard] = useState(false)

  const getListCard = useCallback(async () => {
    try {
      const res = await apiGetListCard()
      if (!res.ok) {
        throw new Error(res.message)
      }
      setListCard(res.data?.cards?.data)
      if (res.data?.cards?.data?.length > 0) {
        handleSelectCard(res.data?.cards?.data[0])
      }
    } catch (e) {}
  }, [])

  useEffect(() => {
    getListCard()
  }, [getListCard])

  useImperativeHandle(ref, () => ({
    setError: message => {
      setErrorMessage(message)
    }
  }))

  function handleSelectCard(card) {
    const {last4, id, brand, exp_month, exp_year, name} = card || {}
    setNumber(last4)
    setExpiryDate(`${exp_month}/${exp_year}`)
    setToken(id)
    setBrand(brand)
    setName(name)
    setUseSaveCard(true)
  }

  function handleClearCard() {
    setNumber('')
    setExpiryDate('')
    setToken('')
    setBrand('')
    setName('')
    setCvc('')
  }

  function handleFormatDate(value) {
    let array = value.split('/')
    if (array[1]?.length > 4) {
      return
    }
    if (array.length === 2 && array[1]?.length <= 4) {
      setExpiryDate(value)
    } else if (array[0].length === 1) {
      setExpiryDate(value)
    } else {
      setExpiryDate(current => {
        if (current.length > value.length) {
          return value
        } else {
          return value + '/'
        }
      })
    }
  }

  return (
    <Modal
      visible={visible}
      transparent={true}
      onRequestClose={() => {}}
      animationType={'fade'}
      animated={true}
    >
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
          backgroundColor: Colors().opacity
        }}
      >
        <ComponentContainer style={styles.container}>
          <ScrollView>
            <View style={{padding: responsiveWidth(15)}}>
              <CardText style={styles.title}>Credit card details</CardText>
              <CardText style={[styles.title, {fontSize: responsiveFont(15)}]}>
                Please enter the card details
              </CardText>
              {!!listCard && !isAddNewCard && (
                <TextInputView
                  value={`${brand} xxxx xxxx xxxx ${number}`}
                  rightIcon={
                    <MaterialIcons
                      name={'keyboard-arrow-down'}
                      size={25}
                      color={Colors().formInputFieldsText}
                    />
                  }
                  onPress={() => setShowSelectCard(true)}
                  editable={false}
                />
              )}
              <TextInputView
                floatTitle={'Name on the card'}
                value={name}
                editable={!isUseSaveCard}
                onChangeText={text => setName(text)}
              />
              <TextInputView
                floatTitle={'Card Number'}
                value={formatCardNumber(+number)}
                editable={!isUseSaveCard}
                maxLength={19}
                keyboardType={'numeric'}
                onChangeText={text => setNumber(text.replace(/-/g, ''))}
              />
              <View style={{flexDirection: 'row'}}>
                <TextInputView
                  floatTitle={'Expiry Date'}
                  style={{flex: 1, marginRight: responsiveWidth(10)}}
                  value={expiryDate}
                  editable={!isUseSaveCard}
                  onChangeText={text => handleFormatDate(text)}
                  maxLength={7}
                />
                <TextInputView
                  floatTitle={'CVC'}
                  style={{flex: 1}}
                  value={cvc}
                  editable={!isUseSaveCard}
                  onChangeText={text => setCvc(text)}
                />
              </View>
              <View style={{flexDirection: 'row'}}>
                <CardText style={styles.textSaveCard}>Would you like to save this card?</CardText>
                <Switch
                  value={isSave}
                  onValueChange={value => setSave(value)}
                  tintColor={Colors().heroText}
                />
              </View>
              {!!errorMessage && (
                <Text style={{color: Colors().errorText, marginTop: responsiveHeight(10)}}>
                  {errorMessage}
                </Text>
              )}
              {!!listCard && (
                <ButtonView
                  title={isAddNewCard ? 'Selected a Saved Card' : 'Add New Card'}
                  style={{
                    marginTop: responsiveHeight(30)
                  }}
                  onPress={() => {
                    setAddNewCard(!isAddNewCard)
                    if (isAddNewCard) {
                      handleSelectCard(listCard[0])
                      setUseSaveCard(true)
                    } else {
                      handleClearCard()
                      setUseSaveCard(false)
                    }
                  }}
                />
              )}
              <CardText style={styles.total}>Total Amount: ${amount.toFixed(2)}</CardText>
            </View>
          </ScrollView>
          <View style={{flexDirection: 'row'}}>
            <NegativeButton title={'Cancel'} style={styles.cancelButton} onPress={onClose} />
            <PositiveButton
              title={'Continue'}
              style={styles.button}
              onPress={() =>
                onSubmit({
                  name,
                  number,
                  cvc,
                  save_card: isSave,
                  use_saved_card: isUseSaveCard,
                  expiryDate,
                  token
                })
              }
            />
          </View>
        </ComponentContainer>
      </View>
      <BottomSheet
        visible={showSelectCard}
        headerTitle={'Select A Card'}
        showConfirmButton={false}
        onClose={() => setShowSelectCard(false)}
      >
        <FlatList
          data={listCard}
          renderItem={({item, index}) => {
            return (
              <TouchableOpacity
                style={[styles.itemCard, {borderColor: Colors().gray}]}
                onPress={() => {
                  handleSelectCard(item)
                  setShowSelectCard(false)
                }}
              >
                <Text
                  style={{
                    fontFamily: Fonts.bold,
                    fontSize: responsiveFont(15)
                  }}
                >{`${item.brand} xxxx xxxx xxxx ${item.last4}`}</Text>
                <Text>Exp: {`${item.exp_month}/${item.exp_year}`}</Text>
              </TouchableOpacity>
            )
          }}
        />
      </BottomSheet>
    </Modal>
  )
}

const styles = StyleSheet.create({
  container: {
    width: deviceWidth() * 0.9,
    borderRadius: responsiveWidth(14),
    overflow: 'hidden'
  },
  button: {
    flex: 1,
    borderRadius: 0
  },
  total: {
    fontSize: responsiveFont(18),
    fontFamily: Fonts.bold,
    textAlign: 'center',
    marginVertical: responsiveHeight(30)
  },
  title: {
    fontSize: responsiveFont(16),
    fontFamily: Fonts.bold,
    textAlign: 'center',
    marginBottom: responsiveHeight(18)
  },
  textSaveCard: {
    fontSize: responsiveFont(14),
    fontFamily: Fonts.regular,
    flex: 1
  },
  itemCard: {
    height: responsiveHeight(50),
    justifyContent: 'center',
    borderBottomWidth: 0.5
  },
  cancelButton: {
    borderRadius: 0,
    borderBottomLeftRadius: responsiveWidth(14),
    flex: 1,
    marginRight: 3
  }
})

export default React.forwardRef(PaymentPopup)
