import React from 'react'
import {View, ScrollView, StyleSheet, Text} from 'react-native'
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import BarcodeBar from './../components/BarcodeBar'
import SubHeaderBar from './../components/SubHeaderBar'
import Styles from '../Themes/Styles'
import Colors from '../Themes/Colors'
import {localize} from '../locale/I18nConfig'
import dayjs from 'dayjs'
import ScreenContainer from '../components/ScreenContainer'
import {CardText} from '../components/Text'

const VoucherDetail = props => {
  const voucher = props?.route?.params?.voucherDetail

  return (
    <ScreenContainer>
      <SubHeaderBar title={localize('voucher.title')} />
      <View style={{flex: 1, paddingHorizontal: responsiveWidth(20)}}>
        <ScrollView>
          {!!voucher?.name && (
            <Text
              style={{
                ...Styles.largeCapBoldText,
                ...{marginTop: responsiveHeight(30), color: Colors().backgroundText}
              }}
            >
              {voucher?.name}
            </Text>
          )}
          {!!voucher?.description && (
            <View style={styles.descContainer}>
              <CardText style={Styles.smallCapText} color={Colors().backgroundText}>
                {voucher?.description}
              </CardText>
            </View>
          )}
          {!!voucher?.barcode && (
            <BarcodeBar
              value={voucher?.barcode}
              width={responsiveWidth(1.5)}
              title={localize('voucher.lookupNumber')}
              text={voucher?.barcode}
              style={{marginTop: responsiveHeight(30)}}
              textColor={Colors().backgroundText}
            />
          )}
          <View style={styles.voucherInfoContainer}>
            {!!voucher?.issueDate && (
              <Text style={[Styles.xSmallCapText, {color: Colors().backgroundText}]}>
                Issue Date: {dayjs(voucher?.issueDate).format('DD/MM/YYYY')}
              </Text>
            )}
            {!!voucher?.expireDate && (
              <Text style={[Styles.xSmallCapText, {color: Colors().backgroundText}]}>
                Expiry Date: {dayjs(voucher?.expireDate).format('DD/MM/YYYY')}
              </Text>
            )}
            {(voucher?.voucherType == '5' ||
              voucher?.voucherType == '10' ||
              voucher?.voucherType == '30') && (
              <Text style={[Styles.xSmallCapText, {color: Colors().backgroundText}]}>
                Remaining Value:{' '}
                {!voucher?.amountLeft
                  ? '$0'
                  : (voucher?.amountLeft / 100).toLocaleString('en-US', {
                      style: 'currency',
                      currency: 'USD'
                    })}
              </Text>
            )}
          </View>
          {!!voucher?.memberBarcode && (
            <BarcodeBar
              value={voucher?.memberBarcode}
              width={1.5}
              title={localize('memberNumber')}
              text={voucher?.memberNumber}
              style={{marginTop: responsiveHeight(100)}}
              textColor={Colors().backgroundText}
            />
          )}
        </ScrollView>
      </View>
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  descContainer: {
    width: '100%',
    marginTop: responsiveHeight(30)
  },
  voucherInfoContainer: {
    width: '100%',
    marginTop: responsiveHeight(30)
  }
})

export default VoucherDetail
