import {formatCardNumber, isEmptyValues, validateEmail} from '../../src/utilities/utils'

describe('Test utils', () => {
  test('validateEmail with valid email', () => {
    expect(validateEmail('hong.h@bepoz.com')).toBe(true)
  })
  test('validateEmail with invalid email', () => {
    expect(validateEmail('hong.hbepoz.com')).toBe(false)
  })
  test('isEmptyValues with empty object', () => {
    expect(isEmptyValues({})).toBe(true)
  })
  test('isEmptyValues with un-empty object', () => {
    expect(isEmptyValues({a: 'value'})).toBe(false)
  })
  test('isEmptyValues with empty string', () => {
    expect(isEmptyValues('')).toBe(true)
  })
  test('isEmptyValues with un-empty string', () => {
    expect(isEmptyValues('value')).toBe(false)
  })
  test('isEmptyValues with undefined', () => {
    expect(isEmptyValues(undefined)).toBe(true)
  })
  test('isEmptyValues with null', () => {
    expect(isEmptyValues(null)).toBe(true)
  })
  test('isEmptyValues with number', () => {
    expect(isEmptyValues(123)).toBe(false)
  })
  test('formatNumber with number 123', () => {
    expect(formatCardNumber(123)).toBe('123')
  })
  test('formatNumber with number 12341234', () => {
    expect(formatCardNumber(12341234)).toBe('1234-1234')
  })

  test('formatNumber with empty', () => {
    expect(formatCardNumber('')).toBe('')
  })
  test('formatNumber with string', () => {
    expect(formatCardNumber('1234-1234')).toBe('1234-1234')
  })
})
